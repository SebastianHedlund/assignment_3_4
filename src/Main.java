import Subject.*;

public class Main {
    public static void main(String[] args) {

        //Create different subjects
        Subject artists = new Subject("artists.txt", "an artist");
        Subject movies = new Subject("movies.txt", "a movie");
        Subject games = new Subject("games.txt", "a game");

        //Add subjects to scalable menu
        Subject[] subjects = new Subject[] {artists, games, movies};
        SubjectMenu myMenu = new SubjectMenu(subjects);

        myMenu.runMenu();

        //It's also possible to easily create separate themed menus:
        Subject[] myHobbies = new Subject[] {movies, games};
        SubjectMenu hobbiesMenu = new SubjectMenu(myHobbies);
        //hobbiesMenu.runMenu();

   }
}
