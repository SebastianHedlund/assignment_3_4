package Subject;

import java.io.*;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Scanner;

public class Subject {
    private String filePath;
    private HashSet<String> newItems = new HashSet<>();
    private LinkedHashSet<String> currentItems = new LinkedHashSet<>();
    private String anItemPrompt;
    private String listItemPrompt;

    public Subject(String filePath, String anItemPrompt){
        this.filePath = filePath;
        this.anItemPrompt = anItemPrompt;
        this.listItemPrompt = "Please type \"exit\" to exit or enter " + anItemPrompt + ": ";
        fetchCurrentItems();
    }
    public Subject(String filePath){
        this.filePath = filePath;
        anItemPrompt = "an item";
        this.listItemPrompt = "Please type \"exit\" to exit or enter " + anItemPrompt + ": ";
        fetchCurrentItems();
    }

    public void printList(){
        //FileReader made just to catch database does not yet exist message
        try (FileReader fileReader = new FileReader(filePath)){
            for (String item : currentItems) {
                System.out.println(item);
            }
        }
        catch (FileNotFoundException e){
            System.out.println(filePath + " database does not yet exist.");
        }
        catch (Exception e){
            System.out.println(e.getMessage());;
        }

    }

    private void fetchCurrentItems(){
        try (FileReader fileReader = new FileReader(filePath);
             BufferedReader bufferedReader = new BufferedReader(fileReader)){

            String line;
            while ((line = bufferedReader.readLine()) != null){
                currentItems.add(line);
            }
        }
        //ignore FileNotFoundException, expected in some cases
        catch (FileNotFoundException e){}
        catch (Exception e){
            System.out.println(e.getMessage());
        }
    }

    public void writeEntreToTemporaryList(){
        String userInput = "";
        Scanner scanner = new Scanner(System.in);

        while(!userInput.equals("exit")){
            System.out.print(listItemPrompt);
            userInput = scanner.nextLine();
            if(!userInput.equals("exit"))
                newItems.add(userInput);
        }
    }

    //Save changes by writing to file
    public void writeToList(){
        try (FileWriter fileWriter = new FileWriter(filePath);
             BufferedWriter bufferedWriter = new BufferedWriter(fileWriter)){

            //Add all newItems to the current currentItems hashSet
            currentItems.addAll(newItems);

            //Rewrite the whole file form a hashSet to make sure no duplicates exist
            for (String item: currentItems) {
                bufferedWriter.write(item + "\n");
            }
        }
        catch (Exception e){
            System.out.println(e.getMessage());
        }
        newItems.clear();
    }

    public void displayCurrentEntries(){
        for (String item : newItems) {
            System.out.println(item);
        }
    }

    public void clearCurrentEntries() {
        newItems.clear();
    }

    public void displayCurrentFileSize() {
        try{
            File databaseFile = new File(filePath);
            if(databaseFile.exists())
                System.out.println((databaseFile.length()) + " Bytes");
        }
        catch (Exception e){
            System.out.println(e.getMessage());
        }
    }

    //Getters and setters
    public String getFilePath(){
        return filePath;
    }

}
