package Subject;

import java.util.HashMap;
import java.util.Scanner;

public class SubjectMenu {
    Subject[] subjects;
    Subject currentSubject;
    HashMap<String, Runnable> subjectFunctionMap = new HashMap<>();
    HashMap<Integer, String> subjectOptionMap = new HashMap<>();
    HashMap<String, String> parentOptionMap = new HashMap<>();
    int listItem = 1;

    public SubjectMenu(Subject[] subjects){
        this.subjects = subjects;
        addOptionsToParentOptionMap();
    }

    public void runMenu(){
        String closeApplicationOption = Integer.toString(parentOptionMap.size());
        String userChoice = "";

        while(true){

            displayParentMenuOptions();
            userChoice = getUserInput();
            if (userChoice.equals(closeApplicationOption)) {
                break;
            }

            try {
                System.out.println();
                currentSubject = subjects[Integer.parseInt(userChoice) - 1];
                addSubjectFunctionsToHashMap();
                runChildMenu();
                subjectOptionMap.clear();
                listItem = 1;
            }
            catch(Exception e){
                System.out.println("Invalid option, please try again.");
            }
            System.out.println();
        }
    }

    private void displayParentMenuOptions(){
        System.out.println("Which database would you like to edit?");
        //print each option from Option hashmap
        for (String option: parentOptionMap.keySet()) {
            System.out.println(option + parentOptionMap.get(option));
        }
    }

    private void addOptionsToParentOptionMap(){
        for (Subject subject : subjects) {
            parentOptionMap.put(String.valueOf(listItem), ". " + subject.getFilePath());
            listItem++;
        }
        parentOptionMap.put(String.valueOf(listItem), ". Close application");
        listItem=1;
    }

    private void addSubjectFunctionsToHashMap(){
        //Add functions & options to hashmaps for the user to select from
        createOption(currentSubject::printList, "Print");
        createOption(currentSubject::writeEntreToTemporaryList, "Write to");
        createOption(currentSubject::displayCurrentEntries, "Display entries made to");
        createOption(currentSubject::clearCurrentEntries, "Clear current entries made to");
        createOption(currentSubject::displayCurrentFileSize, "Display file-size of");
        createOption(currentSubject::writeToList, "Save changes made to");
        //Add default close option at the end of menu
        subjectOptionMap.put(listItem, ". Back");
    }

    private void runChildMenu(){
        String goBack = Integer.toString(subjectOptionMap.size());
        String userChoice = "";

        while(true){

            displayChildMenuOptions();
            userChoice = getUserInput();
            if (userChoice.equals(goBack)) {
                break;
            }
            //Try running the users selected function from Function hashmap
            try {
                subjectFunctionMap.get(userChoice).run();
            }
            catch(NullPointerException e){
                System.out.println("Invalid option, please try again.");
            }
            System.out.println();
        }
    }

    private void displayChildMenuOptions(){
        System.out.println("What would you like to do?");
        //print each option from Option hashmap
        for (Integer option: subjectOptionMap.keySet()) {
            System.out.println(option + subjectOptionMap.get(option));
        }
    }

    private String getUserInput(){
        Scanner scanner = new Scanner(System.in);
        System.out.print("User input: ");
        return scanner.nextLine();
    }

    private void createOption(Runnable function, String prompt){
        subjectFunctionMap.put(String.valueOf(listItem), function);
        subjectOptionMap.put(listItem, ". " + prompt + " " + currentSubject.getFilePath());
        listItem++;
    }

}
